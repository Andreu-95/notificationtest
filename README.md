# Notifications IOS

### Nombre: `Andrés Guerra`
### Grupo: `GR1`

## Pasos Seguidos

- Primero se añadiá un botón al view controller para hacer la llamada a nuestra notificación.
- Después importamos el framework con el comando `import UserNotifications`
- Luego, verificamos que tenemos los permisos necesarios para que, en caso de tenerlos, podamos mostrar la notificación.
- Ahora, añadimos el contenido de nuestra notificación como el título, subtí­tulo, cuerpo e identificador.
- Luego, creamos un trigger que va a ser el encargado de colocar nuestra notificación en la pantalla.
- Luego, realizamos una petición para mostrar nuestra notificación, aquí­ es importante colocar un identificador único para evitar conflictos.
- Finalmente, añadimos nuestra notificación al Notification Center.
- La primera vez que abrimos la app, nos pide autorizar el acceso a las funciones que necesita.
- La notificación se muestra tanto en la pantalla, como en la pantalla de bloqueo.
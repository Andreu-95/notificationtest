//
//  ViewController.swift
//  Notifications
//
//  Created by Andrés Guerra on 3/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    var isGrantedNotificationAccess:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UNUserNotificationCenter.current().requestAuthorization(
            options: [.alert,.sound,.badge],
            completionHandler: { (granted,error) in
                self.isGrantedNotificationAccess = granted
            }
        )
    }

    @IBAction func send10SecNotification(_ sender: Any) {
        if isGrantedNotificationAccess{
            //add notification code here
            let content = UNMutableNotificationContent()
            content.title = "10 Second Notification Demo"
            content.subtitle = "Hecha por Andrés Guerra"
            content.body = "Notification luego de 10 segundos - Es hora del Dota!!"
            content.categoryIdentifier = "message"
            
            let trigger = UNTimeIntervalNotificationTrigger(
                timeInterval: 10.0,
                repeats: false)
            
            let request = UNNotificationRequest(
                identifier: "10.second.message",
                content: content,
                trigger: trigger
            )
            
            UNUserNotificationCenter.current().add(
                request, withCompletionHandler: nil)
        }
    }

}

